import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'calendar-angular';

  datos = [
    {
      id: 1,
      source: {
        id: 1
      },
      target: {
        id: 2
      },
      type: 2
    },
    {
      id: 5,
      source: {
        id: 1
      },
      target: {
        id: 2
      },
      type: 2
    },
    {
      id: 2,
      source: {
        id: 2
      },
      target: {
        id: 4
      },
      type: 1
    },
    {
      id: 3,
      source: {
        id: 1
      },
      target: {
        id: 5
      },
      type: 3
    },
    {
      id: 4,
      source: {
        id: 5
      },
      target: {
        id: 6
      },
      type: 0
    }
  ];

  listaDeEventosOrdenados
  = this.datos.reduce(
      (a: any, b: any) => {
        console.log( 'a', a );
        console.log( 'b', b );

        console.log( 'a[b.source.id]', a[b.source.id] );

      if (!a[b.source.id])
       a[b.source.id] = { registros: [] };

      a[b.source.id].registros.push({... b});
      return a;
    },
  {});

  eventos = [
    {
      id: 1,
      code: "code-007",
      name: "Evento ejemplo 1",
      description: "Descripcion cualquiera...",
      image: "no-img",
      category: 7
    },
    {
      id: 2,
      code: "code-016",
      name: "Evento ejemplo 2",
      description: "Descripcion cualquiera...",
      image: "no-img",
      category: 10
    },
    {
      id: 3,
      code: "code-021",
      name: "Evento ejemplo 3",
      description: "Descripcion cualquiera...",
      image: "no-img",
      category: 3
    },
    {
      id: 4,
      code: "code-089",
      name: "Evento ejemplo 4",
      description: "Descripcion cualquiera...",
      image: "no-img",
      category: 12
    },
    {
      id: 5,
      code: "code-0227",
      name: "Evento ejemplo 5",
      description: "Descripcion cualquiera...",
      image: "no-img",
      category: 1
    },
    {
      id: 6,
      code: "code-007",
      name: "Evento ejemplo 6",
      description: "Descripcion cualquiera...",
      image: "no-img",
      category: 10
    },
    {
      id: 7,
      code: "code-097",
      name: "Evento ejemplo 7",
      description: "Descripcion cualquiera...",
      image: "no-img",
      category: 80
    }
  ];

  obtenerCoincidenciaDeEvento(idEventoActual:any, eventosRelacionados:any) {
    // NUEVO: Si el evento no tiene algun registro retornamos
    // directamente "NO" para rellenar la matriz
    if (!eventosRelacionados) return "-";
    let eventos = eventosRelacionados.registros;

    console.log( 'eventos', eventos );

    //NUEVO: Si hay varias coincidencias
    // retornamos todas.
    let eventosEncontrados = eventos.filter((x: any) => x.target.id == idEventoActual);

    console.log( 'eventosEncontrados', eventosEncontrados );
    return eventosEncontrados.length > 0
      ? eventosEncontrados.reduce((a: any, b: any) => (a += b.type+", "), "")
      : "-";
  }

}
